package programmers;

public abstract class Programmer {

	private String name;
	private String level;
	
	public Programmer(String name, String level){
		this.name = name;
		this.level = level;
	}
	
	public String getname(){
		return this.name;
	}
	
	public String getlevel(){
		return this.level;
	}
	
	public abstract void showskill();
}
